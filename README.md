# **Simple Social Network**

## **Requirements:**
    
    Code Challenge

	    You have received this challenge as part of the recruiting process for HSBC. The contents of this exercise are confidential, so please do not distribute. 
	    You have 7 days to complete this challenge but it shouldn't take you longer than a few hours. Please send it back as soon as you're done. 	

    Description:
	    Build a simple social networking application, similar to Twitter, and expose it through a web API. The application should support the scenarios below. 	

    Scenarios:
	    Posting - A user should be able to post a 140 character message. 	
	    Wall - 	A user should be able to see a list of the messages they've posted, in reverse chronological order. 	
	    Following - A user should be able to follow another user. Following doesn't have to be reciprocal: Alice can follow Bob without Bob having to follow Alice. 	
	    Timeline -  A user should be able to see a list of the messages posted by all the people they follow, in reverse chronological order. 	

    Details:
	    Provide some documentation for the API, so that we know how to use it!  	Don't care about registering users: a user is created as soon as they post their first message.  	Don't care about user authentication.  	Don't care about storage: storing everything in memory is fine. 

    Submitting:
	    Please submit your code as a ZIP archive or a tarball. We'll try to run your submission on a macOS or Linux machine. Please include a README with instructions on how to build and run your project, including any necessary dependencies. Please make sure this process is as straightforward as possible! 	

    Tips: 
	    Please write your solution in a language you feel confident in, provided there is an easy option to build it and run it on macOS/Linux.  	We appreciate people have day jobs and other commitments, so please let us know if you need more time!  	Please do not publish your solution, for example on your blog or source control site.  


## **API Documentation:**
     ```
        1. Posting:
            URL: /api/post
            Method: POST
            URL Params: NA
            Data Params: {"user":"some-user", "message":"message to post"}
            Success Response: 
                "status": 201 (Created)
            Error Response: 
                "status": 400,
                "error": "Bad Request",
                "exception": "java.lang.IllegalArgumentException",
                "message": "Message length should not exceed 140 characters"
            Sample Request: curl -i -H "Content-Type: application/json" -X POST http://localhost:8080/api/post -d "{\"user\":\"Asimo\",\"message\":\"Speech Recognition: A computer pores through millions of audio files and their transcriptions, learns acoustic features for the transcript\"}"
            
        2. Wall:
            URL: /api/wall/{user}
            Method: GET
            URL Params: user (Required)
            Data Params: NA
            Success Response: 
                "status": 200 (OK)
                "body": {
                          "messages": [
                            {
                              "id": "1f9a390e-5a79-45a4-9e78-cc7bf67944bd",
                              "user": "some-user",
                              "message": "message posted"
                            }
                        }
            Error Response: (When user not found) 
                "status": 200 (OK)
                "body": {"messages": []}
            Sample Request: curl -i -X GET http://localhost:8080/api/wall/Asimo
         
        3. Following:
            URL: /api/follow
            Method: POST
            URL Params: NA
            Data Params: {"user":"some-user", "followUser":"some-other-user"}
            Success Response: 
                "status": 201 (Created)
                "body": {"user":"some-user", "followUser":"some-other-user"}
            Error Response: (When tried to follow the same user again)
                "status": 409,
                "error": "Conflict",
                "exception": "org.springframework.dao.DataIntegrityViolationException",
                "message": "User is already being followed",
                "path": "/api/follow"
            Sample Request: curl -i -H "Content-Type: application/json" -X POST http://localhost:8080/api/follow/ -d "{\"user\":\"Asimo\",\"followUser\":\"R2D2\"}"
            
        4. Timeline:
            URL: /api/timeline/{user}
            Method: GET
            URL Params: user (Required)
            Data Params: NA
            Success Response: 
                "status": 200 (OK)
                "body": {
                          "messages": [
                            {
                              "id": "1f9a390e-5a79-45a4-9e78-cc7bf67944bd",
                              "user": "some-following-user",
                              "message": "message posted by following user"
                            }
                        }
            Error Response: (When user not following any one or following users not posted a message) 
                "status": 200 (OK)
                "body": {"messages": []}
            Sample Request: curl -i -X GET http://localhost:8080/api/timeline/Asimo
     ```

## **Instructions To Run:**

    * Project requires Java 8 and Maven to be installed on the computer and the environment variables are set.
    * Download the project, unzip and execute 'cd simple-social-network' in terminal 
    * Execute 'mvn clean install' to compile and build the project
    * 'mvn spring-boot:run' to run the application server
    * Then the Sample Request (see API Documentation) curl commands can be executed 


## **Technical / Implementation Details:**

    * The project uses Java 8, Maven, Spring Boot and other supporting libraries such as H2 in memory database, lombok, cucumber, JUnit and Mockito
     