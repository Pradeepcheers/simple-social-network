package com.code.challenge.simple.social.network.infrastructure.repository.command;

import com.code.challenge.simple.social.network.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * Repository interface to handle post command requests
 */
@Transactional
public interface PostCommandRepository extends JpaRepository<Post, UUID> {

}
