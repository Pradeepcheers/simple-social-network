package com.code.challenge.simple.social.network.actions.wall;

import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for Wall, exposes methods as rest endpoints
 */
@RestController
@RequestMapping(value = "/api/wall")
public class WallController {

    private WallService wallService;

    @RequestMapping(value = "/{user}")
    public ResponseEntity<PostResourceWrapper> getWallPosts(@PathVariable String user) {

        PostResourceWrapper postResourceWrapper = wallService.getWallPosts(user);

        return ResponseEntity.ok(postResourceWrapper);
    }

    @Autowired
    @Required
    public void setWallService(WallService wallService) {
        this.wallService = wallService;
    }
}
