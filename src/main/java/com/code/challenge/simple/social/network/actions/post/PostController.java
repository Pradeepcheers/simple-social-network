package com.code.challenge.simple.social.network.actions.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Controller class for Posting, exposes methods as rest endpoints
 */
@RestController
@RequestMapping("/api/post")
public class PostController {

    private PostService postService;

    @RequestMapping(method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity post(@RequestBody PostResource postResource) {

        UUID messageId = postService.post(postResource);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{messageId}")
                .buildAndExpand(messageId)
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @ExceptionHandler({IllegalArgumentException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Message length should not exceed 140 characters")
    public void illegalArgumentExceptionHandler() {
    }

    @Autowired
    @Required
    public void setPostService(PostService postService) {
        this.postService = postService;
    }
}
