package com.code.challenge.simple.social.network.infrastructure.repository.query;

import com.code.challenge.simple.social.network.model.Follow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository interface to handle follow query requests
 */
@Transactional
public interface FollowQueryRepository extends JpaRepository<Follow, Follow> {
}
