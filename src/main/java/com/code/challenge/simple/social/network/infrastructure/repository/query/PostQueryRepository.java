package com.code.challenge.simple.social.network.infrastructure.repository.query;

import com.code.challenge.simple.social.network.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * Repository interface to handle post query requests
 */
@Transactional
public interface PostQueryRepository extends JpaRepository<Post, UUID> {

    List<Post> findByUserOrderByPostedDateTimeDesc(String user);

    @Query(value = "select p from Post p where p.user in (select f.followUser from Follow f where f.user = :user) order by p.postedDateTime desc")
    List<Post> findByFollowingUsersOrderByPostedDateTimeDesc(@Param("user") String user);
}
