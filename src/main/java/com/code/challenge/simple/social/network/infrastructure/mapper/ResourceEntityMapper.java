package com.code.challenge.simple.social.network.infrastructure.mapper;

import com.code.challenge.simple.social.network.actions.follow.FollowResource;
import com.code.challenge.simple.social.network.actions.post.PostResource;
import com.code.challenge.simple.social.network.model.Follow;
import com.code.challenge.simple.social.network.model.Post;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Helper class transforms Entity object to Resource object and vice-versa
 */
@Component
public class ResourceEntityMapper {
    public Post map(PostResource postResource) {

        return Post.builder()
                .user(postResource.getUser())
                .message(postResource.getMessage())
                .build();
    }

    public PostResource map(Post post) {

        return PostResource.builder()
                .id(post.getId().toString())
                .user(post.getUser())
                .message(post.getMessage())
                .build();
    }

    public List<PostResource> map(List<Post> posts) {
        return posts.stream().map(this::map).collect(Collectors.toList());
    }

    public FollowResource map(Follow follow) {
        return FollowResource.builder().user(follow.getUser()).followUser(follow.getFollowUser()).build();
    }

    public Follow map(FollowResource followResource) {
        return Follow.builder().user(followResource.getUser()).followUser(followResource.getFollowUser()).build();
    }
}
