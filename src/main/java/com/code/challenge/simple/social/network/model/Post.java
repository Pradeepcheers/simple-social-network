package com.code.challenge.simple.social.network.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "POST")
public class Post implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID id;

    @Column(nullable = false)
    private String user;

    @Column(nullable = false, length = 140)
    private String message;

    @Column(nullable = false, columnDefinition="TIMESTAMP")
    private LocalDateTime postedDateTime;

    @PrePersist
    protected void onCreate() {
        postedDateTime = LocalDateTime.now();
    }
}
