package com.code.challenge.simple.social.network.actions.follow;

import com.code.challenge.simple.social.network.infrastructure.mapper.ResourceEntityMapper;
import com.code.challenge.simple.social.network.infrastructure.repository.command.FollowCommandRepository;
import com.code.challenge.simple.social.network.model.Follow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

import static org.springframework.util.Assert.hasText;
import static org.springframework.util.Assert.notNull;

@Service
public class FollowServiceImpl implements FollowService {

    private FollowCommandRepository followCommandRepository;

    private ResourceEntityMapper resourceEntityMapper;

    public FollowResource follow(FollowResource followResource) {
        notNull(followResource, "Follow resource cannot be null");
        hasText(followResource.getUser(), "User cannot be null or empty");
        hasText(followResource.getFollowUser(), "User to follow cannot be null or empty");

        Follow follow = resourceEntityMapper.map(followResource);

        Follow savedFollow = followCommandRepository.save(follow);

        return resourceEntityMapper.map(savedFollow);
    }

    @Autowired
    @Required
    public void setFollowCommandRepository(FollowCommandRepository followCommandRepository) {
        this.followCommandRepository = followCommandRepository;
    }

    @Autowired
    @Required
    public void setResourceEntityMapper(ResourceEntityMapper resourceEntityMapper) {
        this.resourceEntityMapper = resourceEntityMapper;
    }
}
