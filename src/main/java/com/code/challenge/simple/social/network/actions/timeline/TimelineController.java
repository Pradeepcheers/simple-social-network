package com.code.challenge.simple.social.network.actions.timeline;

import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for Timeline, exposes methods as rest endpoints
 */
@RestController
@RequestMapping(value = "/api/timeline")
public class TimelineController {

    private TimelineService timelineService;

    @RequestMapping(value = "/{user}")
    public ResponseEntity<PostResourceWrapper> getTimelinePosts(@PathVariable String user) {

        PostResourceWrapper postResourceWrapper = timelineService.getTimelinePosts(user);

        return ResponseEntity.ok(postResourceWrapper);
    }

    @Autowired
    @Required
    public void setTimelineService(TimelineService timelineService) {
        this.timelineService = timelineService;
    }
}
