package com.code.challenge.simple.social.network.actions.timeline;

import com.code.challenge.simple.social.network.actions.post.PostResource;
import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;
import com.code.challenge.simple.social.network.infrastructure.mapper.ResourceEntityMapper;
import com.code.challenge.simple.social.network.infrastructure.repository.query.PostQueryRepository;
import com.code.challenge.simple.social.network.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.util.Assert.hasText;

@Service
public class TimelineServiceImpl implements TimelineService {

    private PostQueryRepository postQueryRepository;

    private ResourceEntityMapper resourceEntityMapper;

    @Override
    public PostResourceWrapper getTimelinePosts(String user) {
        hasText(user, "User cannot be null or empty");

        List<Post> posts = postQueryRepository.findByFollowingUsersOrderByPostedDateTimeDesc(user);

        List<PostResource> postResources = resourceEntityMapper.map(posts);

        return PostResourceWrapper.builder().messages(postResources).build();
    }

    @Autowired
    @Required
    public void setPostQueryRepository(PostQueryRepository postQueryRepository) {
        this.postQueryRepository = postQueryRepository;
    }

    @Autowired
    @Required
    public void setResourceEntityMapper(ResourceEntityMapper resourceEntityMapper) {
        this.resourceEntityMapper = resourceEntityMapper;
    }
}
