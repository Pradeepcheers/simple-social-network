package com.code.challenge.simple.social.network.actions.follow;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Resource to represent an user is following another user
 */
@Data
@Builder
@AllArgsConstructor
public class FollowResource {

    private String user;

    private String followUser;
}
