package com.code.challenge.simple.social.network.actions.post;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Resource acts as a response object wraps a list of {@code PostResponse}
 */
@Data
@AllArgsConstructor
@Builder
public class PostResourceWrapper {

    private List<PostResource> messages = new ArrayList<>();
}
