package com.code.challenge.simple.social.network.actions.follow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Controller class for Following, exposes methods as rest endpoints
 */
@RestController
@RequestMapping(value = "/api/follow")
public class FollowController {

    private FollowService followService;

    @RequestMapping(method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<FollowResource> follow(@RequestBody FollowResource followResource) {

        FollowResource followingResource = followService.follow(followResource);

        return new ResponseEntity<>(followingResource, HttpStatus.CREATED);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "User is already being followed")
    public void dataIntegrityViolationException() {
    }

    @Autowired
    @Required
    public void setFollowService(FollowServiceImpl followService) {
        this.followService = followService;
    }
}
