package com.code.challenge.simple.social.network.actions.post;

import com.code.challenge.simple.social.network.infrastructure.mapper.ResourceEntityMapper;
import com.code.challenge.simple.social.network.infrastructure.repository.command.PostCommandRepository;
import com.code.challenge.simple.social.network.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.springframework.util.Assert.*;

@Service
public class PostServiceImpl implements PostService {

    private PostCommandRepository postCommandRepository;

    private ResourceEntityMapper resourceEntityMapper;

    private final Predicate<String> isValidMessage = m -> m.length() <= 140;

    @Override
    public UUID post(PostResource postResource) {
        validateMessage.apply(postResource);

        Post post = resourceEntityMapper.map(postResource);

        Post savedPost = postCommandRepository.save(post);

        return savedPost.getId();
    }

    private final Function<PostResource, Boolean> validateMessage = postingResource -> {
        notNull(postingResource, "Posting resource cannot be null");
        hasText(postingResource.getUser(), "User cannot be null or empty");
        hasText(postingResource.getMessage(), "Message cannot be null or empty");
        isTrue(isValidMessage.test(postingResource.getMessage()), "Message length should not exceed 140 characters");

        return true;
    };

    @Autowired
    @Required
    public void setPostingRepository(PostCommandRepository postCommandRepository) {
        this.postCommandRepository = postCommandRepository;
    }

    @Autowired
    @Required
    public void setResourceEntityMapper(ResourceEntityMapper resourceEntityMapper) {
        this.resourceEntityMapper = resourceEntityMapper;
    }
}
