package com.code.challenge.simple.social.network.actions.wall;

import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;

/**
 * Service interface to handle Wall related queries
 */
public interface WallService {

    /**
     * Retrieves posts posted by the user (Wall) in reverse chronological order
     *
     * @param user posts for the user
     * @return {@link PostResourceWrapper}
     */
    PostResourceWrapper getWallPosts(String user);
}
