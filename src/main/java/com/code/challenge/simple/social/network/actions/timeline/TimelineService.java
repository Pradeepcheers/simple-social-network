package com.code.challenge.simple.social.network.actions.timeline;

import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;

/**
 * Service interface to handle Timeline related queries
 */
public interface TimelineService {

    /**
     * Retrieves posts posted by all following users (Timeline) in reverse chronological order
     *
     * @param user posts for the user
     * @return {@link PostResourceWrapper}
     */
    PostResourceWrapper getTimelinePosts(String user);
}
