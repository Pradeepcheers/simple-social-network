package com.code.challenge.simple.social.network.infrastructure.repository.command;

import com.code.challenge.simple.social.network.model.Follow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository interface to handle follow command requests
 */
@Transactional
public interface FollowCommandRepository extends JpaRepository<Follow, Follow> {
}
