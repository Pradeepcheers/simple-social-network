package com.code.challenge.simple.social.network.actions.post;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Resource representing a simple social network message
 */
@Data
@Builder
@AllArgsConstructor
public class PostResource {

    private String id;

    private String user;

    private String message;
}
