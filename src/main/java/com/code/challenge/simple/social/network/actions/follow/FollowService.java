package com.code.challenge.simple.social.network.actions.follow;

/**
 * Service manages simple social network users follow other users
 */
public interface FollowService {

    FollowResource follow(FollowResource followResource);
}
