package com.code.challenge.simple.social.network.actions.post;

import java.util.UUID;

/**
 * Service handles simple social network message post
 */
public interface PostService {

    UUID post(PostResource postResource);

}
