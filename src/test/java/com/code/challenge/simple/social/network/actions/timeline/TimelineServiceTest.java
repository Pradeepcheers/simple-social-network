package com.code.challenge.simple.social.network.actions.timeline;


import com.code.challenge.simple.social.network.actions.post.PostResource;
import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;
import com.code.challenge.simple.social.network.infrastructure.mapper.ResourceEntityMapper;
import com.code.challenge.simple.social.network.infrastructure.repository.query.PostQueryRepository;
import com.code.challenge.simple.social.network.model.Post;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TimelineServiceTest {

    private static final String USER = "someUser";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private TimelineServiceImpl timelineService;

    @Mock
    private PostQueryRepository postQueryRepository;

    @Mock
    private ResourceEntityMapper resourceEntityMapper;

    @Test
    public void getTimelinePosts_shouldReturnEmptyPostsWhenThereAreNoPostsForTheFollowingUser() {
        when(postQueryRepository.findByFollowingUsersOrderByPostedDateTimeDesc(USER)).thenReturn(new ArrayList<>());

        PostResourceWrapper postResourceWrapper = timelineService.getTimelinePosts(USER);

        assertThat(postResourceWrapper, is(notNullValue()));
        assertThat(postResourceWrapper.getMessages(), hasSize(0));
    }

    @Test
    public void getTimelinePosts_shouldReturnPostsPostedByTheFollowingUsers() {
        List<Post> posts = Lists.newArrayList(buildPost("C", "C"), buildPost("B", "B"), buildPost("A", "A"));
        when(postQueryRepository.findByFollowingUsersOrderByPostedDateTimeDesc(USER)).thenReturn(posts);

        List<PostResource> postResources = Lists.newArrayList(buildPostingResource("C","C"), buildPostingResource("B", "B"), buildPostingResource("A","A"));
        when(resourceEntityMapper.map(posts)).thenReturn(postResources);

        PostResourceWrapper postResourceWrapper = timelineService.getTimelinePosts(USER);

        assertThat(postResourceWrapper, is(notNullValue()));
        assertThat(postResourceWrapper.getMessages(), hasSize(3));
        assertThat(postResourceWrapper.getMessages(), is(postResources));
    }

    @Test
    public void getWallPosts_shouldThrowExceptionWhenUserIsNullOrEmpty() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("User cannot be null or empty");

        timelineService.getTimelinePosts("");
    }

    private Post buildPost(String user, String message) {
        return Post.builder().id(UUID.randomUUID()).user(user).message(message).build();
    }

    private PostResource buildPostingResource(String user, String message) {
        return PostResource.builder().user(user).message(message).build();
    }
}
