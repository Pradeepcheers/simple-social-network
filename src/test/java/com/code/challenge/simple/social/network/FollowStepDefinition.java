package com.code.challenge.simple.social.network;

import com.code.challenge.simple.social.network.actions.follow.FollowResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = Application.class)
public class FollowStepDefinition {

    private static final String URI = "http://localhost:8080";

    private static final String PATH = "/api/follow";

    private ObjectMapper mapper;

    private TestRestTemplate testRestTemplate;

    private ResponseEntity<FollowResource> entity;

    @Before
    public void setup() {
        testRestTemplate = new TestRestTemplate();
        mapper = new ObjectMapper();
    }

    @When("^user '([^\"]*)' request to follow user '([^\"]*)'$")
    public void userRequestToFollowUser(String user, String followUser) throws Throwable {
        FollowResource followResource = FollowResource.builder().user(user).followUser(followUser).build();

        entity = testRestTemplate.postForEntity(URI + PATH, followResource, FollowResource.class);

        assertThat(entity, is(notNullValue()));
    }

    @Then("^user '([^\"]*)' should be able to follow user '([^\"]*)'$")
    public void userShouldBeAbleToFollowUser(String user, String followUser) throws Throwable {
        assertThat(entity.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(entity.getBody().getUser(), is(user));
        assertThat(entity.getBody().getFollowUser(), is(followUser));
    }
}
