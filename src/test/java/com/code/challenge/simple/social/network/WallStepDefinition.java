package com.code.challenge.simple.social.network;

import com.code.challenge.simple.social.network.actions.post.PostResource;
import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = Application.class)
public class WallStepDefinition {

    private static final String URI = "http://localhost:8080";

    private static final String PATH = "/api/wall/";

    private TestRestTemplate testRestTemplate;

    private ResponseEntity<PostResourceWrapper> entity;

    @Before
    public void setup() {
        testRestTemplate = new TestRestTemplate();
    }

    @When("^a user '([^\"]*)' makes a request to show wall$")
    public void aUserAMakesARequestToShowWall(String user) throws Throwable {
        entity = testRestTemplate.getForEntity(URI + PATH + user, PostResourceWrapper.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getStatusCode(), is(HttpStatus.OK));
    }

    @Then("^an empty message list is returned$")
    public void anEmptyMessageListIsReturned() throws Throwable {
        PostResourceWrapper postResourceWrapper = entity.getBody();

        assertThat(postResourceWrapper, is(notNullValue()));
        assertThat(postResourceWrapper.getMessages(), hasSize(0));
    }

    @Then("^return a message list posted by the user in reverse chronological order$")
    public void returnAMessageListPostedByTheUserInReverseChronologicalOrder() throws Throwable {

    }

    @Then("^return a message list in the reverse chronological order posted by the user as follows$")
    public void returnAMessageListInTheReverseChronologicalOrderPostedByTheUserAsFollows(List<PostResource> postResources) throws Throwable {
        PostResourceWrapper postResourceWrapper = entity.getBody();

        assertThat(postResourceWrapper, is(notNullValue()));
        assertThat(postResourceWrapper.getMessages(), hasSize(4));
        IntStream.range(0, 4).forEach(i -> {
            assertThat(postResourceWrapper.getMessages().get(i).getUser(), is(postResources.get(i).getUser()));
            assertThat(postResourceWrapper.getMessages().get(i).getMessage(), is(postResources.get(i).getMessage()));
        });
    }
}
