package com.code.challenge.simple.social.network.actions.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.UUID;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
public class PostControllerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @InjectMocks
    private PostController postController;

    @Mock
    private PostService postService;

    @Before
    public void init() {
        mockMvc = standaloneSetup(postController).build();
    }

    @Test
    public void post_shouldPostAMessage() throws Exception {
        UUID messageId  = UUID.randomUUID();

        PostResource postResource = PostResource.builder()
                .user("Test User")
                .message("Artificial-intelligence system surfs web to improve its performance")
                .build();

        RequestBuilder requestBuilder = post("/api/post")
                .content(mapper.writeValueAsString(postResource))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .accept(APPLICATION_JSON_UTF8_VALUE);

        when(postService.post(postResource)).thenReturn(messageId);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(header().string("location", equalTo("http://localhost/api/post/"+messageId)));
    }

    @Test
    public void post_shouldThrowExceptionWhenMessageLengthIsNotValid() throws Exception {
        PostResource postResource = PostResource.builder()
                .user("Test User")
                .message("Researchers have designed a machine-learning system that implemented their model, and they trained it to recognize particular faces by feeding it a battery of sample images")
                .build();

        RequestBuilder requestBuilder = post("/api/post")
                .content(mapper.writeValueAsString(postResource))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .accept(APPLICATION_JSON_UTF8_VALUE);

        when(postService.post(postResource)).thenThrow(new IllegalArgumentException("Message length should not exceed 140 characters"));

        mockMvc.perform(requestBuilder)
                .andExpect(status().is4xxClientError());
    }
}
