package com.code.challenge.simple.social.network;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = { "pretty", "html:target/cucumber" },
                 features = "src/test/resources/",
                 glue = "com.code.challenge.simple.social.network")
public class FeaturesRunner {
}
