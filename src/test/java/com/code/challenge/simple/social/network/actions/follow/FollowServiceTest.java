package com.code.challenge.simple.social.network.actions.follow;

import com.code.challenge.simple.social.network.infrastructure.mapper.ResourceEntityMapper;
import com.code.challenge.simple.social.network.infrastructure.repository.command.FollowCommandRepository;
import com.code.challenge.simple.social.network.model.Follow;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FollowServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private FollowServiceImpl followService;

    @Mock
    private FollowCommandRepository followCommandRepositoryRepository;

    @Mock
    private ResourceEntityMapper resourceEntityMapper;

    @Test
    public void follow_shouldAllowUserToFollowAnotherUser() {
        FollowResource followResource = FollowResource.builder().user("A").followUser("B").build();

        Follow follow = Follow.builder().user("A").followUser("B").build();

        when(followCommandRepositoryRepository.save(follow)).thenReturn(follow);
        when(resourceEntityMapper.map(followResource)).thenReturn(follow);
        when(resourceEntityMapper.map(follow)).thenReturn(followResource);

        FollowResource followingResource = followService.follow(followResource);

        assertThat(followingResource, is(notNullValue()));
        assertThat(followingResource, is(followResource));
    }

    @Test
    public void follow_shouldThrowExceptionWhenFollowResourceIsNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Follow resource cannot be null");

        followService.follow(null);
    }

    @Test
    public void follow_shouldThrowExceptionWhenUserIsNullOrEmpty() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("User cannot be null or empty");

        followService.follow(FollowResource.builder().user("").followUser("B").build());
    }

    @Test
    public void follow_shouldThrowExceptionWhenUserToFollowIsNullOrEmpty() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("User to follow cannot be null or empty");

        followService.follow(FollowResource.builder().user("A").followUser("").build());
    }
}
