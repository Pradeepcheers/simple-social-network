package com.code.challenge.simple.social.network;

import com.code.challenge.simple.social.network.actions.post.PostResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = Application.class)
public class PostStepDefinition {

    private static final String URI = "http://localhost:8080";

    private static final String PATH = "/api/post";

    private ObjectMapper mapper;

    private TestRestTemplate testRestTemplate;

    private ResponseEntity<String> entity;

    @Before
    public void setup() {
        testRestTemplate = new TestRestTemplate();
        mapper = new ObjectMapper();
    }

    @Given("^the service is up and running$")
    public void theServiceIsUpAndRunning() throws Throwable {
        ResponseEntity<String> entity = testRestTemplate.getForEntity(URI + "/health", String.class);

        assertThat(entity, is(notNullValue()));
        assertThat(entity.getStatusCode(), is(HttpStatus.OK));
        assertThat(entity.getBody(), hasJsonPath("$.status", is(Status.UP.toString())));
    }

    @When("^the following message is posted")
    public void iPostTheFollowingMessage(final String jsonInputAsString) throws Throwable {
        PostResource postResource = mapper.readValue(jsonInputAsString, PostResource.class);

        entity = testRestTemplate.postForEntity(URI + PATH, postResource, String.class);

        assertThat(entity, is(notNullValue()));
    }

    @Then("^an exception is thrown indicating bad request$")
    public void anExceptionIsThrownIndicatingBadRequest() throws Throwable {
        assertThat(entity.getStatusCode(), is(HttpStatus.BAD_REQUEST));
    }

    @Then("^the message should be successfully posed$")
    public void theMessageShouldBeSuccessfullyPosed() throws Throwable {
        assertThat(entity.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(entity.getHeaders().get("location").stream().findFirst().get(), containsString(URI + PATH));
    }
}
