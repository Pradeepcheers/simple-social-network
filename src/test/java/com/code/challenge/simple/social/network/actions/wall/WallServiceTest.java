package com.code.challenge.simple.social.network.actions.wall;

import com.code.challenge.simple.social.network.actions.post.PostResource;
import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;
import com.code.challenge.simple.social.network.infrastructure.mapper.ResourceEntityMapper;
import com.code.challenge.simple.social.network.infrastructure.repository.query.PostQueryRepository;
import com.code.challenge.simple.social.network.model.Post;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WallServiceTest {

    private static final String USER = "some-user";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private WallServiceImpl wallService;

    @Mock
    private PostQueryRepository postQueryRepository;

    @Mock
    private ResourceEntityMapper resourceEntityMapper;

    @Test
    public void getWallPosts_shouldReturnEmptyPostsWhenThereAreNoPostsForTheUser() {
        when(postQueryRepository.findByUserOrderByPostedDateTimeDesc(USER)).thenReturn(new ArrayList<>());

        PostResourceWrapper postResourceWrapper = wallService.getWallPosts(USER);

        assertThat(postResourceWrapper, is(notNullValue()));
        assertThat(postResourceWrapper.getMessages(), hasSize(0));
    }

    @Test
    public void getWallPosts_shouldReturnPostsPostedByTheUser() {
        List<Post> posts = Lists.newArrayList(buildPost("C"), buildPost("B"), buildPost("A"));
        when(postQueryRepository.findByUserOrderByPostedDateTimeDesc(USER)).thenReturn(posts);

        List<PostResource> postResources = Lists.newArrayList(buildPostingResource("C"), buildPostingResource("B"), buildPostingResource("A"));
        when(resourceEntityMapper.map(posts)).thenReturn(postResources);

        PostResourceWrapper postResourceWrapper = wallService.getWallPosts(USER);

        assertThat(postResourceWrapper, is(notNullValue()));
        assertThat(postResourceWrapper.getMessages(), hasSize(3));
        assertThat(postResourceWrapper.getMessages(), is(postResources));
    }

    @Test
    public void getWallPosts_shouldThrowExceptionWhenUserIsNullOrEmpty() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("User cannot be null or empty");

        wallService.getWallPosts("");
    }

    private Post buildPost(String message) {
        return Post.builder().id(UUID.randomUUID()).user(USER).message(message).build();
    }

    private PostResource buildPostingResource(String message) {
        return PostResource.builder().user(USER).message(message).build();
    }
}
