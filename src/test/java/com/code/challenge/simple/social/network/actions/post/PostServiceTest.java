package com.code.challenge.simple.social.network.actions.post;

import com.code.challenge.simple.social.network.infrastructure.mapper.ResourceEntityMapper;
import com.code.challenge.simple.social.network.infrastructure.repository.command.PostCommandRepository;
import com.code.challenge.simple.social.network.model.Post;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private PostServiceImpl postingService;

    @Mock
    private PostCommandRepository postingRepository;

    @Mock
    private ResourceEntityMapper resourceEntityMapper;

    @Test
    public void post_shouldReturnPostedMessageId() {
        UUID savedMessageId = UUID.randomUUID();

        PostResource postResource = PostResource.builder().user("test user").message("test message").build();

        Post post = Post.builder().id(savedMessageId).user("test user").message("test message").build();

        when(resourceEntityMapper.map(postResource)).thenReturn(post);
        when(postingRepository.save(post)).thenReturn(post);

        UUID messageId = postingService.post(postResource);

        assertThat(messageId, is(notNullValue()));
        assertThat(messageId, is(savedMessageId));
    }

    @Test
    public void post_shouldReturnPostedMessageIdWhenMessageLengthIs140Characters() {
        UUID savedMessageId = UUID.randomUUID();
        String message = "Deep-learning algorithm predicts photos at “near-human” levels. Artificial Intelligence Lab could help with teaching, and memory improvement";

        PostResource postResource = PostResource.builder().user("test user").message(message).build();

        Post post = Post.builder().id(savedMessageId).user("test user").message(message).build();

        when(resourceEntityMapper.map(postResource)).thenReturn(post);
        when(postingRepository.save(post)).thenReturn(post);

        UUID messageId = postingService.post(postResource);

        assertThat(messageId, is(notNullValue()));
        assertThat(messageId, is(savedMessageId));
    }

    @Test
    public void post_shouldThrowExceptionWhenPostingResourceIsNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Posting resource cannot be null");

        postingService.post(null);
    }

    @Test
    public void post_shouldThrowExceptionWhenUserIsEmpty() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("User cannot be null or empty");

        PostResource postResource = PostResource.builder()
                .user("")
                .message("test message")
                .build();

        postingService.post(postResource);
    }

    @Test
    public void post_shouldThrowExceptionWhenMessageIsEmpty() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Message cannot be null or empty");

        PostResource postResource = PostResource.builder()
                .user("test user")
                .message("")
                .build();

        postingService.post(postResource);
    }

    @Test
    public void post_shouldThrowExceptionWhenMessageLengthExceedsValidLimitOf140Characters() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Message length should not exceed 140 characters");

        PostResource postResource = PostResource.builder()
                .user("test user")
                .message("Deep-learning algorithm predicts photos at “near-human” levels. Artificial Intelligence Lab could help with teaching, and memory improvement.")
                .build();

        postingService.post(postResource);
    }
}
