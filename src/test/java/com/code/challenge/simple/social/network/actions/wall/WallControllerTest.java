package com.code.challenge.simple.social.network.actions.wall;

import com.code.challenge.simple.social.network.actions.post.PostResourceWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
public class WallControllerTest {
    
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();
    
    @InjectMocks
    private WallController wallController;

    @Mock
    private WallService wallService;

    @Before
    public void init() {
        mockMvc = standaloneSetup(wallController).build();
    }
    
    @Test
    public void getWallPosts_shouldRetrieveMessagesPostedBySpecifiedUser() throws Exception {
        PostResourceWrapper postResourceWrapper = PostResourceWrapper.builder().build();

        RequestBuilder requestBuilder = get("/api/wall/some-user")
                .accept(APPLICATION_JSON_UTF8_VALUE);

        when(wallService.getWallPosts("some-user")).thenReturn(postResourceWrapper);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(postResourceWrapper)));
    }
}
