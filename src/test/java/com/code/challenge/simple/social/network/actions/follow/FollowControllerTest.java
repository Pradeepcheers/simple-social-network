package com.code.challenge.simple.social.network.actions.follow;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
public class FollowControllerTest {

    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @InjectMocks
    private FollowController followingController;

    @Mock
    private FollowService followService;

    @Before
    public void init() {
        mockMvc = standaloneSetup(followingController).build();
    }

    @Test
    public void follow_shouldBeAbleToFollowAnotherUser() throws Exception {
        FollowResource followResource = FollowResource.builder().build();

        RequestBuilder requestBuilder = post("/api/follow")
                .content(mapper.writeValueAsString(followResource))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .accept(APPLICATION_JSON_UTF8_VALUE);

        when(followService.follow(followResource)).thenReturn(followResource);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(content().string(mapper.writeValueAsString(followResource)));
    }

    @Test
    public void follow_shouldThrowConflictWhenUserIsAlreadyFollowingTheUser() throws Exception {
        FollowResource followResource = FollowResource.builder().build();

        RequestBuilder requestBuilder = post("/api/follow")
                .content(mapper.writeValueAsString(followResource))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .accept(APPLICATION_JSON_UTF8_VALUE);

        when(followService.follow(followResource)).thenReturn(followResource);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(content().string(mapper.writeValueAsString(followResource)));

        when(followService.follow(followResource)).thenThrow(new DataIntegrityViolationException("User is already being followed"));

        mockMvc.perform(requestBuilder)
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(containsString("User is already being followed")));
    }
}
