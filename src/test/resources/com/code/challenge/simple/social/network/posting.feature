Feature: As a user I want to be able to post a 140 character message

  Scenario: Posting a message greater than 140 characters
    Given the service is up and running
    When the following message is posted
    """
      {
        "user":"test user",
        "message":"Speech Recognition: A computer pores through millions of audio files and their transcriptions, learns which acoustic features correspond to which typed words"
      }
    """
    Then an exception is thrown indicating bad request

  Scenario: Posting a message less than 140 characters
    Given the service is up and running
    When the following message is posted
    """
      {
        "user":"test user",
        "message":"Speech Recognition: A computer pores through millions of audio files and their transcriptions, learns acoustic features for the transcript"
      }
    """
    Then the message should be successfully posed
