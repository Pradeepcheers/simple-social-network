Feature: As a user I want to be able to see a list of the messages posted by all the users I follow, in reverse chronological order

  Scenario: Empty message list when no messages are posted by the following users
    Given the service is up and running
    When a user 'S' makes a request to show wall
    Then an empty message list is returned

  Scenario: List all the messages posted by the following users and verify the order of messages are in reverse chronological order
    Given the service is up and running
    And user 'S' request to follow user 'X'
    And user 'S' request to follow user 'Y'
    And user 'S' request to follow user 'Z'
    And the following message is posted
    """
      {
        "user":"X",
        "message":"Artificial intelligence produces realistic sounds that fool humans"
      }
    """
    And the following message is posted
    """
      {
        "user":"X",
        "message":"Building better trust between humans and machines"
      }
    """
    And the following message is posted
    """
      {
        "user":"Y",
        "message":"Computer system passes Visual Turing test!"
      }
    """
    And the following message is posted
    """
      {
        "user":"Z",
        "message":"Deep learning can teach machines to predict the future"
      }
    """
    When a user 'S' makes a request to show the timeline
    Then return a message list in the reverse chronological order posted by the following users
      | user | message                                                            |
      | Z    | Deep learning can teach machines to predict the future             |
      | Y    | Computer system passes Visual Turing test!                         |
      | X    | Building better trust between humans and machines                  |
      | X    | Artificial intelligence produces realistic sounds that fool humans |