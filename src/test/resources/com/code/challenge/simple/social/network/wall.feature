Feature: As a user I want to be able to see a list of the messages I've posted, in reverse chronological order

  Scenario: Empty message list when no messages are posted
    Given the service is up and running
    When a user 'A' makes a request to show wall
    Then an empty message list is returned

  Scenario: List all the messages posted by the user and verify the order of messages are in reverse chronological order
    Given the service is up and running
    And the following message is posted
    """
      {
        "user":"B",
        "message":"Artificial intelligence produces realistic sounds that fool humans"
      }
    """
    And the following message is posted
    """
      {
        "user":"B",
        "message":"Building better trust between humans and machines"
      }
    """
    And the following message is posted
    """
      {
        "user":"B",
        "message":"Computer system passes Visual Turing test!"
      }
    """
    And the following message is posted
    """
      {
        "user":"B",
        "message":"Deep learning can teach machines to predict the future"
      }
    """
    When a user 'B' makes a request to show wall
    Then return a message list in the reverse chronological order posted by the user as follows
    | user | message                                                            |
    | B    | Deep learning can teach machines to predict the future             |
    | B    | Computer system passes Visual Turing test!                         |
    | B    | Building better trust between humans and machines                  |
    | B    | Artificial intelligence produces realistic sounds that fool humans |

