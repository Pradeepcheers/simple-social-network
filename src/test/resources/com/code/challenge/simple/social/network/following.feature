Feature: As a user I want to be able to follow another user but following doesn't have to be reciprocal

  Scenario: User should be able to follow another user
    Given the service is up and running
    When user 'A' request to follow user 'B'
    Then user 'A' should be able to follow user 'B'